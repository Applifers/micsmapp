package ph.com.applife.smapp.Utilities;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;




import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;

import ph.com.applife.smapp.SMdashboard;

/**
 * Created by Oclemy on 6/5/2016 for ProgrammingWizards Channel and http://www.camposha.com.
 */
public class Downloader extends AsyncTask<Void,Void,String> {


    Context c;
    String urlAddress;

    ListView lv2;
    Session session;
    ProgressDialog pd;

    // private PreferenceHelper preferenceHelper;

    public Downloader(Context c, String urlAddress ,ListView lv2) {
        this.c = c;
        this.urlAddress = urlAddress;

        this.lv2 = lv2;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //preferenceHelper = new PreferenceHelper(c);
        pd=new ProgressDialog(c);
        pd.setTitle("Retrieve");
        pd.setMessage("Retrieving...Please wait");
        pd.show();
        //  Toast.makeText(c,preferenceHelper.getChannelname(),Toast.LENGTH_SHORT);
    }

    @Override
    protected String doInBackground(Void... params) {
        session = new Session(c);
        HashMap<String,String> HashMapParams = new HashMap<String,String>();
        HashMapParams.put("user",session.getUsername());
        String response="";
        try {
            HttpRequest req = new HttpRequest(urlAddress);
            response = req.prepare(HttpRequest.Method.POST).withData(HashMapParams).sendAndReadString();
        } catch (Exception e) {
            response=e.getMessage();
        }
        return response;
    }


    //return downloadData();


    @Override
    protected void onPostExecute(String jsonData) {
        super.onPostExecute(jsonData);

        pd.dismiss();

        if(jsonData==null)
        {
            Toast.makeText(c,"Unsuccessful,No data Retrieved",Toast.LENGTH_SHORT).show();
        }else {
            //PARSE
            DataParser parser=new DataParser(c,jsonData,lv2);
            parser.execute();

        }
    }

    private String downloadData()
    {
        HttpURLConnection con=Connector.connect(urlAddress);
        if(con==null)
        {
            return null;
        }

        try
        {
            InputStream is=new BufferedInputStream(con.getInputStream());
            BufferedReader br=new BufferedReader(new InputStreamReader(is));

            String line;
            StringBuffer jsonData=new StringBuffer();


            while ((line=br.readLine()) != null)
            {
                jsonData.append(line+"\n");
            }

            br.close();
            is.close();

            return jsonData.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }
}
