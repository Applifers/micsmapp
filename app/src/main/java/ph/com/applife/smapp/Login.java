package ph.com.applife.smapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;

import ph.com.applife.smapp.Utilities.ApplifeUtils;
import ph.com.applife.smapp.Utilities.HttpRequest;
import ph.com.applife.smapp.Utilities.ParseContent;
import ph.com.applife.smapp.Utilities.Session;

public class Login extends AppCompatActivity {
    Button signin,signup;
    CheckBox checkBoxShowPwd;
    EditText passEditText,userEditText;
    Session session;
    ParseContent parseContent;
    final static String login= "http://applife.com.ph/SMInventory/Login.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        signin = (Button)findViewById(R.id.signin);
        signup = (Button)findViewById(R.id.signup);
        passEditText = (EditText)findViewById(R.id.password);
        userEditText = (EditText)findViewById(R.id.username);

        session = new Session(this);
        parseContent = new ParseContent(this);
        if(session.getloggedin()){
            Intent intent = new Intent(Login.this, SMdashboard.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            this.finish();
        }

        //checkbox show pass
        checkBoxShowPwd = (CheckBox)findViewById(R.id.checkBox);
        checkBoxShowPwd.setText("Show password"); // Hide initially, but prompting "Show Password"
        checkBoxShowPwd.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
                if (isChecked) {
                    passEditText.setTransformationMethod(null); // Show password when box checked
                    checkBoxShowPwd.setText("Hide Password"); // Prompting "Hide Password"
                } else {
                    passEditText.setTransformationMethod(new PasswordTransformationMethod()); // Hide password when box not checked
                    checkBoxShowPwd.setText("Show Password"); // Prompting "Show Password"
                }
            }
        } );


        //sign in
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ApplifeUtils.isNetworkAvailable(Login.this)) {
                    Toast.makeText(Login.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                    return;
                }
                ApplifeUtils.showSimpleProgressDialog(Login.this);
                final HashMap<String, String> map = new HashMap<>();
                map.put("USERNAME", userEditText.getText().toString());
                map.put("PASSWORD", passEditText.getText().toString());
                new AsyncTask<Void, Void, String>(){
                    protected String doInBackground(Void[] params) {
                        String response="";
                        try {
                            HttpRequest req = new HttpRequest(login);
                            response = req.prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                        } catch (Exception e) {
                            response=e.getMessage();
                        }
                        return response;
                    }
                    protected void onPostExecute(String result) {
                        //do something with response
                        Log.d("newwwss", result);
                        onTaskCompleted(result,1);
                    }
                }.execute();
            }

            private void onTaskCompleted(String response,int task) {
                Log.d("responsejson", response.toString());
                ApplifeUtils.removeSimpleProgressDialog();  //will remove progress dialog
                switch (task) {
                    case 1:
                        if (parseContent.isSuccess(response)) {
                            session.setLoggedin(true);
                            parseContent.saveInfo(response);
                            Toast.makeText(Login.this, "Login Successfully!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Login.this,SMdashboard.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            Login.this.finish();
                        }else {
                            Toast.makeText(Login.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
                        }
                }
            }


        });


        //sign up
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, Registration.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Login.this.finish();
            }
        });

    }

}
