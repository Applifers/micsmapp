package ph.com.applife.smapp.Utilities;

import android.view.ContextMenu;
import android.view.View;
import android.widget.TextView;

import ph.com.applife.smapp.R;


/**
 * Created by Oclemmy on 5/5/2016 for ProgrammingWizards Channel and http://www.Camposha.com.
 */
public class MyViewHolder implements View.OnLongClickListener {

   TextView branch,sku,quantity,expdate,status,stocks,modified,onhand;

    MyLongClickListener longClickListener;

    public MyViewHolder(View v) {
        branch= (TextView) v.findViewById(R.id.branch);
        sku= (TextView) v.findViewById(R.id.sku);
        quantity= (TextView) v.findViewById(R.id.quantity);
        expdate= (TextView) v.findViewById(R.id.expdate);
        stocks= (TextView) v.findViewById(R.id.stocks);
        status= (TextView) v.findViewById(R.id.status);
        onhand= (TextView) v.findViewById(R.id.onhand);
        modified = (TextView) v.findViewById(R.id.modified);


        v.setOnLongClickListener(this);
       // v.setOnCreateContextMenuListener(this);
    }

    @Override
    public boolean onLongClick(View v) {
        this.longClickListener.onItemLongClick();
        return false;
    }

    public void setLongClickListener(MyLongClickListener longClickListener)
    {
        this.longClickListener=longClickListener;
    }

   /* @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle("Action : ");
        menu.add(0, 0, 0, "NEW");
        menu.add(0,1,0,"EDIT");
        menu.add(0,2,0,"DELETE");


    }*/
}