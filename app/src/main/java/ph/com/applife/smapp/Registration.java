package ph.com.applife.smapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;

import ph.com.applife.smapp.Utilities.ApplifeUtils;
import ph.com.applife.smapp.Utilities.HttpRequest;
import ph.com.applife.smapp.Utilities.ParseContent;
import ph.com.applife.smapp.Utilities.Session;

public class Registration extends AppCompatActivity {
        Button signin,back;
        EditText fname,unames,pass,pass2;
        final static String register= "http://applife.com.ph/SMInventory/Registration.php";
        private final int RegTask = 1;
        Session session;
        ParseContent parseContent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        signin = (Button)findViewById(R.id.signin);
        back = (Button)findViewById(R.id.back);
        fname = (EditText)findViewById(R.id.fullname);
        unames = (EditText)findViewById(R.id.username);
        pass = (EditText)findViewById(R.id.password);
        pass2 = (EditText)findViewById(R.id.confirmpassword);
        session = new Session(this);
        parseContent = new ParseContent(this);

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{


                    if (unames.getText().toString().trim().length() == 0 ) {
                        unames.setError("Please enter username!");
                        unames.requestFocus();
                        return;
                    }

                    if (unames.getText().toString().trim().length() < 4 ) {
                        unames.setError("Username must be more than 4 character!");
                        unames.requestFocus();
                        return;
                    }

                    if (pass.getText().toString().trim().length() == 0) {
                        pass.setError("Please enter Password!");
                        pass.requestFocus();
                        return;
                    }


                    if (pass.getText().toString().trim().length() < 4) {
                        pass.setError("Password must be more than 4 character!");
                        pass.requestFocus();
                        return;
                    }
                    if (!pass.getText().toString().equals(pass2.getText().toString())) {
                        pass2.setError("Password not match");
                        pass2.requestFocus();
                        return;
                    }

                    if (fname.getText().toString().trim().length() == 0) {
                        fname.setError("Please enter Name");
                        fname.requestFocus();
                        return;
                    }


                    if (unames.getText().toString().trim().length() == 0){
                        fname.setError("Please enter name!");
                        fname.requestFocus();
                        return;
                    }


                    reg();
                    // Toast.makeText(getApplicationContext(),"pinindot", Toast.LENGTH_LONG).show();

                }catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Registration.this, Login.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Registration.this.finish();
            }
        });


    }

    private void reg() throws IOException, JSONException {
        if (!ApplifeUtils.isNetworkAvailable(Registration.this)) {
            Toast.makeText(Registration.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }

        ApplifeUtils.showSimpleProgressDialog(Registration.this);
        final HashMap<String, String> map = new HashMap<>();

        map.put("USERNAME", unames.getText().toString());
        map.put("PASSWORD",  pass.getText().toString());
        map.put("FULLNAME",  fname.getText().toString());
        new AsyncTask<Void, Void, String>() {
            protected String doInBackground(Void[] params) {
                String response = "";
                try {
                    HttpRequest req = new HttpRequest(register);
                    response = req.prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response = e.getMessage();
                }
                return response;
            }

            protected void onPostExecute(String result) {
                //do something with response
                Log.d("newwwss", result);
                onTaskCompleted(result, RegTask);
            }
        }.execute();
    }


    private void onTaskCompleted(String response,int task) {
        Log.d("responsejson", response.toString());
        ApplifeUtils.removeSimpleProgressDialog();  //will remove progress dialog
        switch (task) {
            case RegTask:
                if (parseContent.isSuccess(response)) {
                    parseContent.saveInfo(response);

                        session.setLoggedin(true);
                        Toast.makeText(Registration.this, "Register Success!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Registration.this,SMdashboard.class);
                        // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        this.finish();
                    }

                else {
                    Toast.makeText(Registration.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
                }
        }
    }

}
