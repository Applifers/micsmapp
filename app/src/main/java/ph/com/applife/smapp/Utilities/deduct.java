package ph.com.applife.smapp.Utilities;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ph.com.applife.smapp.Login;
import ph.com.applife.smapp.R;
import ph.com.applife.smapp.SMdashboard;

public class deduct extends AppCompatActivity {
    public static int stocks;
    Dialog d;
    CalendarView calendarView;
    int year;
    int month, day;
    String date,item,item2;
    EditText quantity;
    Spinner spinner,branch,ssm,svi,smco,allday;
    Button submit,view;
    ParseContent parseContent;
    Session session;
    boolean selected, branchselect;
    final static String add= "http://applife.com.ph/SMInventory/itemsold.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deduct);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        spinner = findViewById(R.id.spinner);
        ssm = findViewById(R.id.ssm);
        svi = findViewById(R.id.svi);
        smco = findViewById(R.id.smco);
        allday = findViewById(R.id.allday);
        branch =  findViewById(R.id.spinner2);
        quantity = (EditText) findViewById(R.id.quantitysold);


        parseContent = new ParseContent(this);
        session = new Session(this);
        List<String> categories = new ArrayList<>();
        List<String> SSM = new ArrayList<>();
        List<String> SVI = new ArrayList<>();
        List<String> SMCO = new ArrayList<>();
        List<String> BRANCHES = new ArrayList<>();
        List<String> ALLDAY = new ArrayList<>();
        categories.add(0, "Choose SKU");
        categories.add("SSM-BLACK FOREST");
        categories.add("SSM-BUKO PANDA");
        categories.add("SSM-CHEESE");
        categories.add("SSM-CHOCO TRUFFLES");
        categories.add("SSM-CHOCOLATE");
        categories.add("SSM-COOKIES & CREAM");
        categories.add("SSM-DOUBLE DUTCH");
        categories.add("SSM-MANGO");
        categories.add("SSM-STRAWBERRY");
        categories.add("SSM-UBE");
        categories.add("SSM-VANILLA");
        categories.add("SSK-BLACK FOREST");
        categories.add("SSK-BUKO PANDAN");
        categories.add("SSK-COOKIES & CREAM");
        categories.add("SSK-ROCKY ROAD");
        categories.add("SSK-STRAWBERRY");
        categories.add("SSK-TARO UBE");
        categories.add("STB-CAPPUCINO");
        categories.add("STB-CHOCO TRUFFLES");
        categories.add("STB-DOUBLE DUTCH");
        categories.add("STB-MOCHA");
        categories.add("BVG-LEMON ICE TEA");
        categories.add("BVG-RED ICE TEA");
        categories.add("BVG-BLACK GULAMAN");
        categories.add("BKY-WHIPPING CREAM");
        categories.add("BKY-HOT CAKE");
        categories.add("BKY-ORIGINAL WAFFLE");
        categories.add("SVR-CHICKEN JOYFUL");
        categories.add("SDF-CHOCOLATE SYRUP");
        categories.add("SDF-CARAMEL SYRUP");
        categories.add("SDF-STRAWBERRY SYRUP");
        categories.add("MCF-3IN1 COFFEE");
        categories.add("MCF-3IN1 CHOCO");
        categories.add("MCF-3IN1 COFFEE MIX");
        categories.add("MCF-HOT CHOCO");
        categories.add("MCF-CAPPUCCINU MIX");
        categories.add("MCF-CARAMEL");
        SSM.add("SSM ADRIATICO");
        SSM.add("SSM ANTIPOLO");
        SSM.add("SSM BALIWAG");
        SSM.add("SSM BATANGAS");
        SSM.add("SSM BICUTAN");
        SSM.add("SSM CAINTA");
        SSM.add("SSM CEBU");
        SSM.add("SSM CENTER PASIG");
        SSM.add("SSM CENTER VALENZUELA");
        SSM.add("SSM CHERRY FOODERAMA");
        SSM.add("SSM CLARK PAMPANGA");
        SSM.add("SSM CUBAO");
        SSM.add("SSM ETON");
        SSM.add("SSM FAIRVIEW");
        SSM.add("SSM FTI");
        SSM.add("SSM HANDUMANAN");
        SSM.add("SSM ILO ILO");
        SSM.add("SSM IMUS");
        SSM.add("SSM JAZZ MAKATI");
        SSM.add("SSM IMALL CEBU");
        SSM.add("SSM LAPU LAPU");
        SSM.add("SSM LAS PIÑAS CENTER");
        SSM.add("SSM LAS PIÑAS ZAPOTE");
        SSM.add("SSM MAKATI");
        SSM.add("SSM MALL OF ASIA");
        SSM.add("SSM MANDALUYONG");
        SSM.add("SSM MARILAO");
        SSM.add("SSM MARKET MALL DASMA");
        SSM.add("SSM MOLINO");
        SSM.add("SSM MONUMENTO");
        SSM.add("SSM MUNTINLUPA");
        SSM.add("SSM NORTH EDSA");
        SSM.add("SSM PAMPANGA");
        SSM.add("SSM ROSALES");
        SSM.add("SSM SUCAT");
        SSM.add("SSM SUCAT E-SERVICE ROAD");
        SSM.add("SSM SUCAT LOPEZ");
        SSM.add("SSM TAYTAY");
        SSM.add("SSM DASMARIÑAS");
        SSM.add("SSM ESR");
        SVI.add("SVI AURA PREMIERE");
        SVI.add("SVI B.F PARAÑAQUE");
        SVI.add("SVI BACOOR");
        SVI.add("SVI BAGUIO");
        SVI.add("SVI BATANGAS");
        SVI.add("SVI CALAMBA LAGUNA");
        SVI.add("SVI CEBU RECLAMATION");
        SVI.add("SVI CONSOLACION");
        SVI.add("SVI CUBAO");
        SVI.add("SVI DAVAO");
        SVI.add("SVI FAIRVIEW");
        SVI.add("SVI GENSAN");
        SVI.add("SVI LANANG DAVAO");
        SVI.add("SVI LIPA");
        SVI.add("SVI MANILA");
        SVI.add("SVI MARIKINA");
        SVI.add("SVI MEGAMALL");
        SVI.add("SVI PAMPANGA");
        SVI.add("SVI SAN LAZARO");
        SVI.add("SVI SEASIDE CEBU");
        SVI.add("SVI SOUTHMALL");
        SVI.add("SVI STA. MESA");
        SMCO.add("SMCO AMIGO CABANATUAN");
        SMCO.add("SMCO ARANETA CENTER");
        SMCO.add("SMCO AVENIDA");
        SMCO.add("SMCO BANLIC LAGUNA");
        SMCO.add("SMCO BINALONAN");
        SMCO.add("SMCO BROADWAY");
        SMCO.add("SMCO CAMARINE");
        SMCO.add("SMCO DAVAO BANGKAL");
        SMCO.add("SMCO FESTIVAL MALL");
        SMCO.add("SMCO FORTUNE TOWN");
        SMCO.add("SMCO FREE CHOICE PASIG");
        SMCO.add("SMCO GUAGUA PAMPANGA");
        SMCO.add("SMCO LAONGLAAN");
        SMCO.add("SMCO LATRINIDAD");
        SMCO.add("SMCO M ALVAREZ");
        SMCO.add("SMCO MEGA CENTER");
        SMCO.add("SMCO MENDEZ TAGAYTAY");
        SMCO.add("SMCO MEZZA");
        SMCO.add("SMCO NOVA PLAZA");
        SMCO.add("SMCO NOVALICHES");
        SMCO.add("SMCO PARKMALL");
        SMCO.add("SMCO STA. MARIA BULACAN");
        SMCO.add("SMCO TACLOBAN");
        SMCO.add("SMCO TANAY");
        SMCO.add("SMCO ZABARTE");
        SMCO.add("SMCO CUBAO");
        SMCO.add("SMCO MARIKINA");

        BRANCHES.add("ALL DAY SUPERMARKET");
        BRANCHES.add("MERRY MART GROCERY CENTERS INC.");
        BRANCHES.add("FORTUNE MART");
        BRANCHES.add("STA. LUCIA SUPERMARKET");
        BRANCHES.add("PASIG SUPERMARKET");
        BRANCHES.add("SUPER SHOPPING MARKET");
        BRANCHES.add("SUPER VALUE INC");
        BRANCHES.add("SANFORD MARKETING CORPORATION");
        BRANCHES.add("LE PROFESSIONAL");
        ALLDAY.add("AD EVIA SUPERMARKET");
        ALLDAY.add("AD TANZA SUPERMARKET");
        ALLDAY.add("AD GEN TRIAS SUPERMARKET");
        ALLDAY.add("AD GLOBAL SOUTH SUPERMARKET");
        ALLDAY.add("AD LAS PIÑAS SUPERMARKET");
        ALLDAY.add("AD KAWIT SUPERMARKET");
        ALLDAY.add("AD MOLINO SUPERMARKET");
        ALLDAY.add("AD STA. ROSA SUPERMARKET");
        ALLDAY.add("AD TAGUIG SUPERMARKET");
        ALLDAY.add("AD SHAW SUPERMARKET");
        ALLDAY.add("AD LIBIS SUPERMARKET");
        ALLDAY.add("AD DC ALABANG");
        ALLDAY.add("AD NAGA BRANCH");
        ALLDAY.add("AD BATAAN BRANCH");
        ALLDAY.add("AD PAMPANGA BRANCH");
        ALLDAY.add("AD ILOILO BRANCH");
        ALLDAY.add("AD MALOLOS BRANCH");
        ALLDAY.add("AD ALABANG BRANCH");
        ALLDAY.add("AD AUGUSTINE BRANCH");

        ArrayAdapter<String> dataAdapter;
        dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1,categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).equals("Choose SKU")) {
                    selected = false;
                }

                else {
                    item = parent.getItemAtPosition(position).toString();
                    selected = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<String> branchesdata;
        branchesdata = new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1,BRANCHES);
        branchesdata.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        branch.setAdapter(branchesdata);
        branch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).equals("Choose BRANCH")) {


                }
                if (parent.getItemAtPosition(position).equals("SUPER SHOPPING MARKET")) {
                    ssm.setVisibility(View.VISIBLE);
                    svi.setVisibility(View.GONE);
                    smco.setVisibility(View.GONE);
                    allday.setVisibility(View.GONE);
                    return;
                }
                if (parent.getItemAtPosition(position).equals("SUPER VALUE INC")) {
                    ssm.setVisibility(View.GONE);
                    svi.setVisibility(View.VISIBLE);
                    smco.setVisibility(View.GONE);
                    allday.setVisibility(View.GONE);
                    return;
                }
                if (parent.getItemAtPosition(position).equals("SANFORD MARKETING CORPORATION")) {
                    ssm.setVisibility(View.GONE);
                    svi.setVisibility(View.GONE);
                    smco.setVisibility(View.VISIBLE);
                    allday.setVisibility(View.GONE);
                    return;
                }
                if (parent.getItemAtPosition(position).equals("ALL DAY SUPERMARKET")) {
                    ssm.setVisibility(View.GONE);
                    svi.setVisibility(View.GONE);
                    smco.setVisibility(View.GONE);
                    allday.setVisibility(View.VISIBLE);
                    return;
                }


                else {
                    item2 = parent.getItemAtPosition(position).toString();
                    selected = true;
                    branchselect = true;
                    ssm.setVisibility(View.GONE);
                    svi.setVisibility(View.GONE);
                    smco.setVisibility(View.GONE);
                    allday.setVisibility(View.GONE);

                }
            }




            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> ssmdata;
        ssmdata = new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1,SSM);
        ssmdata.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        ssm.setAdapter(ssmdata);
        ssm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).equals("Choose")) {
                    selected = false;
                }

                else {
                    item2 = parent.getItemAtPosition(position).toString();
                    selected = true;
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> svidata;
        svidata = new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1,SVI);
        svidata.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        svi.setAdapter(svidata);
        svi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).equals("Choose")) {
                    selected = false;
                }

                else {
                    item2 = parent.getItemAtPosition(position).toString();
                    selected = true;
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> smcodata;
        smcodata = new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1,SMCO);
        smcodata.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        smco.setAdapter(smcodata);
        smco.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).equals("Choose")) {
                    selected = false;
                }

                else {
                    item2 = parent.getItemAtPosition(position).toString();
                    selected = true;
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> alldayadata;
        alldayadata = new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1,ALLDAY);
        alldayadata.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        allday.setAdapter(alldayadata);
        allday.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).equals("Choose")) {
                    selected = false;
                }

                else {
                    item2 = parent.getItemAtPosition(position).toString();
                    selected = true;
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        submit = (Button)findViewById(R.id.submit);
        view = (Button)findViewById(R.id.view);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    if(!selected){
                        Toast.makeText(deduct.this, "choose sku", Toast.LENGTH_SHORT).show();
                        return;
                    }


                    if (quantity.getText().toString().trim().length() == 0) {
                        quantity.setError("Please enter Item sold!");
                        quantity.requestFocus();
                        return;
                    }



                    else
                        sendEmailmonth();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(deduct.this, SMdashboard.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                deduct.this.finish();
            }
        });



    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(deduct.this,SMdashboard.class);
        // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        this.finish();
    }

    private void sendEmailmonth() throws IOException, JSONException {
        if (!ApplifeUtils.isNetworkAvailable(deduct.this)) {
            Toast.makeText(deduct.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }

        ApplifeUtils.showSimpleProgressDialog(deduct.this);
        final HashMap<String, String> map = new HashMap<>();

        map.put("USERNAME", session.getUsername());
        map.put("SKU", item);
        map.put("BRANCH",item2 );
        map.put("SOLD",quantity.getText().toString());




        new AsyncTask<Void, Void, String>() {
            protected String doInBackground(Void[] params) {
                String response = "";
                try {
                    HttpRequest req = new HttpRequest(add);
                    response = req.prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response = e.getMessage();
                }
                return response;
            }

            protected void onPostExecute(String result) {
                //do something with response
                Log.d("newwwss", result);
                onTaskCompleteds(result, 1);
            }
        }.execute();
    }
    private void onTaskCompleteds(String response, int task) {
        Log.d("responsejson", response.toString());
        ApplifeUtils.removeSimpleProgressDialog();  //will remove progress dialog
        switch (task) {
            case 1:
                if (parseContent.isSuccess(response)) {
                    //parseContent.saveInfo(response);
                    quantity.setText("");


                    Toast.makeText(deduct.this, "Success!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(deduct.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
                }
        }
    }
}