package ph.com.applife.smapp.Utilities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import ph.com.applife.smapp.R;
import ph.com.applife.smapp.skulist.Listdashboard;
/**
 * Created by Oclemmy on 5/5/2016 for ProgrammingWizards Channel and http://www.Camposha.com.
 */
public class CustomAdapter extends BaseAdapter {

    private final int RegTask = 1;
    Context c;
    ArrayList<Spacecraft> spacecrafts;
    LayoutInflater inflater;


    public static boolean hide;
    ImageView image;
    private Session session;
    ParseContent parseContent;
    Dialog d;
    String sname;

    public CustomAdapter(Context c, ArrayList<Spacecraft> spacecrafts) {
        this.c = c;
        this.spacecrafts = spacecrafts;

    }

    @Override
    public int getCount() {
        return spacecrafts.size();
    }

    @Override
    public Object getItem(int position) {
        return spacecrafts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(inflater==null)
        {
            inflater= (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView==null)
        {
            convertView=inflater.inflate(R.layout.model,parent,false);
        }

        //BIND DATA



        // Float f1= Float.parseFloat(vote);


        MyViewHolder holder=new MyViewHolder(convertView);
        //listdasboard click
        if (hide){
            holder.branch.setText(spacecrafts.get(position).getBranch());
            holder.sku.setText("SKU: "+spacecrafts.get(position).getSku());
            holder.quantity.setText("Delivered Stocks: " +spacecrafts.get(position).getQuantity());
            holder.expdate.setText("Exp date: "+spacecrafts.get(position).getExpdate());
            holder.status.setText("Status: "+spacecrafts.get(position).getStatus());
            holder.stocks.setText("Stocks: "+spacecrafts.get(position).getStocks());
            holder.modified.setText("Modified: "+spacecrafts.get(position).getModified());
            holder.onhand.setText("Stock On Hand: "+spacecrafts.get(position).getOnhand());
            holder.branch.setVisibility(View.GONE);
            holder.sku.setVisibility(View.VISIBLE);
            holder.quantity.setVisibility(View.VISIBLE);
            holder.expdate.setVisibility(View.VISIBLE);
            holder.status.setVisibility(View.VISIBLE);
            holder.stocks.setVisibility(View.VISIBLE);
            holder.modified.setVisibility(View.VISIBLE);
            holder.onhand.setVisibility(View.VISIBLE);

        }
        //maindashboard
        else {
            holder.branch.setText(spacecrafts.get(position).getBranch());
            holder.branch.setVisibility(View.VISIBLE);
            holder.sku.setText(spacecrafts.get(position).getSku());
            holder.sku.setVisibility(View.GONE);
            holder.quantity.setVisibility(View.GONE);
            holder.expdate.setVisibility(View.GONE);
            holder.status.setVisibility(View.GONE);
            holder.stocks.setVisibility(View.GONE);
            holder.modified.setVisibility(View.GONE);
            holder.onhand.setVisibility(View.GONE);
        }
        //Toast.makeText(c, ""+spacecrafts.get(position).getUsername(), Toast.LENGTH_SHORT).show();
        //String path = spacecrafts.get(position).getImageUrl();
        parseContent = new ParseContent((Activity) c);
        //image.setImageURI(Uri.parse(spacecrafts.get(position).getImageUrl()));


        final Spacecraft spacecraft = spacecrafts.get(position);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(c, spacecrafts.get(position).getUsername(), Toast.LENGTH_SHORT).show();

                hide = true;
                Listdashboard.sku=spacecrafts.get(position).getBranch();
                add.stocks=spacecrafts.get(position).getStocks();
                Intent intent = new Intent(c, Listdashboard.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                c.startActivity(intent);





            }







                });





        holder.setLongClickListener(new MyLongClickListener() {
            @Override
            public void onItemLongClick() {
                Spacecraft spacecraft = (Spacecraft) getItem(position);
              /*  MainActivity.id=spacecrafts.get(position).getId();
                MainActivity.name=spacecrafts.get(position).getName();
                MainActivity.dept=spacecrafts.get(position).getDepartment();
                MainActivity.post=spacecrafts.get(position).getPosition();
                MainActivity.image=spacecrafts.get(position).getImageUrl();
                MainActivity.rate=spacecrafts.get(position).getStars();*/

            }
        });

        return convertView;
    }

    //EXPOSE NAME AND ID



}