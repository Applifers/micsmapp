package ph.com.applife.smapp.skulist;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import ph.com.applife.smapp.R;
import ph.com.applife.smapp.Registration;
import ph.com.applife.smapp.SMdashboard;
import ph.com.applife.smapp.Utilities.CustomAdapter;
import ph.com.applife.smapp.Utilities.Downloader;
import ph.com.applife.smapp.Utilities.ParseContent;
import ph.com.applife.smapp.Utilities.Session;
import ph.com.applife.smapp.Utilities.Spacecraft;
import ph.com.applife.smapp.Utilities.add;

public class Listdashboard extends AppCompatActivity {
    public static String sku;
    public static ListView lv2;
    public static String passsession;

    Session session;
    Dialog d;
    Spacecraft spacecraft;
    ParseContent parseContent;
    String urlAddress = "http://applife.com.ph/SMInventory/retrievesku.php";
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Listdashboard.this,SMdashboard.class);
        // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listdashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        session = new Session(this);
        spacecraft = new Spacecraft();
        CustomAdapter.hide=true;
        lv2 = (ListView) findViewById(R.id.lv2);
        View emptyView = findViewById(R.id.empty_view);
        lv2.setEmptyView(emptyView);
        parseContent = new ParseContent(this);
        new Downloader2(Listdashboard.this, urlAddress, lv2).execute();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Intent intent = new Intent(Listdashboard.this, add.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Listdashboard.this.finish();
                }

        });




    }

}
