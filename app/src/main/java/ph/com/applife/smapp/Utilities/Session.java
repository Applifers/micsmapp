package ph.com.applife.smapp.Utilities;

import android.content.Context;
import android.content.SharedPreferences;

public class Session {
    SharedPreferences prefs;
    // SharedPreferences.Editor editor;
    Context ctx;
    private final String Username = "user";
    private final String Fullname = "fname";
    private final String Password = "password";





    public Session(Context ctx){

        prefs = ctx.getSharedPreferences("myapp", Context.MODE_PRIVATE);
        this.ctx = ctx;
        //editor = prefs.edit();
    }



    public void setLoggedin(boolean logggedin){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("loggedInmode",logggedin);
        editor.commit();
    }

    public boolean getloggedin(){
        return prefs.getBoolean("loggedInmode", false);

    }


    public void setUsername(String user){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Username,user);
        editor.commit();
    }
    public String getUsername() {
        return prefs.getString(Username, "");
    }

    public void setPassword(String password){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Password,password);
        editor.commit();
    }
    public String getPassword() {
        return prefs.getString(Password, "");
    }

    public void putfname(String fname) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Fullname, fname);
        editor.commit();
    }
    public String getfname(){return prefs.getString(Fullname,"");
    }



}

