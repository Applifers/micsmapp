package ph.com.applife.smapp.Utilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;




import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class DataParser extends AsyncTask<Void,Void,Integer> {

    Context c;
    String jsonData;

    ListView lv2;


    ProgressDialog pd;
    ArrayList<Spacecraft> spacecrafts=new ArrayList<>();


    public DataParser(Context c, String jsonData,ListView lv2) {
        this.c = c;
        this.jsonData = jsonData;
        this.lv2 = lv2;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        pd=new ProgressDialog(c);
        pd.setTitle("Parse");
        pd.setMessage("Parsing...Please wait");
        pd.show();
    }

    @Override
    protected Integer doInBackground(Void... params) {




        return this.parseData();
    }

    @Override
    protected void onPostExecute(Integer result) {
        super.onPostExecute(result);

        pd.dismiss();

        if(result==0)
        {
            //  Toast.makeText(c,"Unable To Parse",Toast.LENGTH_SHORT).show();
        }else {
            //BIND DATA TO LISTVIEW
            CustomAdapter adapter=new CustomAdapter(c,spacecrafts);

            lv2.setAdapter(adapter);
        }
    }

    private int parseData()
    {
        try
        {
            JSONArray ja=new JSONArray(jsonData);
            JSONObject jo=null;
            spacecrafts.clear();
            Spacecraft spacecraft;

            for(int i=0;i<ja.length();i++)
            {
                jo=ja.getJSONObject(i);

                int id=jo.getInt("id");
                String name=jo.getString("Username");
                String branch=jo.getString("Branch");
                String sku=jo.getString("ItemInventory");
                int quantity=jo.getInt("Quantity");
                int onhand=jo.getInt("StockonHand");
                int stocks = jo.getInt("RemainingStocks");
                String expdate=jo.getString("Expiration");
                String status=jo.getString("Status");
                String modified=jo.getString("DateInserted");


                spacecraft=new Spacecraft();
                spacecraft.setId(id);
                spacecraft.setUsername(name);
                spacecraft.setBranch(branch);
                spacecraft.setSku(sku);
                spacecraft.setQuantity(quantity);
                spacecraft.setOnhand(onhand);
                spacecraft.setStocks(stocks);
                spacecraft.setExpdate(expdate);
                spacecraft.setStatus(status);
                spacecraft.setModified(modified);

                spacecrafts.add(spacecraft);
            }

            return 1;


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }
}

